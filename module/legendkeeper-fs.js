export class LegendKeeperFs {
  constructor() {
    let path = game.settings.get('legendkeeper-integration', 'importDirectory');
    path = path && path[0] != '/' ? `/${path}` : path;

    this.origin = window.location.origin;
    // TODO: Make this dynamic.
    this.source = `user`;
    this.path = path ? path : '';
    this.index = null;
    this.tree = [];
    this.sorted = {};
    this.handled = [];
    this.parents = [];
    this.dialog = null;
    this.scheduledUpdates = [];
    this.killAll = false;
  }

  async fetchJson(filename, skipHandled = true) {
    if (this.killAll) {
      return false;
    }

    let response = null;
    // Handle imports
    if (!skipHandled || this.handled.includes(filename) == false) {
      try {
        response = await fetch(`${this.origin}${filename[0] != '/' ? '/' : ''}${filename}`);
        if (response.status !== 200) {
          ui.notifications.error(game.i18n.localize('LK_INTEGRATION.error.noIndex'));
          return false;
        }
        else {
          let json = await response.json();
          if (!json.rootLocale) {
            this.handled.push(json.id);
            this.tree.push(json);
          }
          return json;
        }
      } catch (error) {
        ui.notifications.error(game.i18n.localize('LK_INTEGRATION.error.noIndex'));
        return false;
      }
    }
    return false;
  }

  async fetchIndex() {
    let index = await this.fetchJson(`${this.path}/index.json`, false);

    if (index?.rootLocale?.id) {
      await this.fetchJson(`${this.path}/${index.rootLocale.id}.json`);
      this.index = index.rootLocale.id;
      return true;
      // this.tree[index.rootLocale.id] = indexJson;
    }
    else {
      return false;
    }
  }

  sortEntries() {
    // Handle top-level first.
    let result = {};
    result[this.index] = {
      content: this.tree.find(entry => entry.id == this.index),
      items: []
    };

    let sort = game.settings.get('legendkeeper-integration', 'sortOnImport');

    this.tree.filter(entry => entry.parentId == null).forEach(entry => {
      this.queryTree(entry, result[this.index]['items'], sort);
    });

    if (sort) {
      result[this.index]['items'] = result[this.index]['items'].sort((a, b) => {
        const aSort = a.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');
        const bSort = b.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');

        if (aSort < bSort) {
          return -1;
        }
        if (aSort > bSort) {
          return 1;
        }
        return 0;
      });
    }

    return result;
  }

  queryTree(entry, sortedEntries, sort = false) {
    if (this.killAll) {
      return false;
    }

    let items = this.tree.filter(item => item.parentId == entry.id);
    sortedEntries.push({
      content: entry,
      items: []
    });

    let index = sortedEntries.length - 1;

    items.forEach(item => {
      let children = this.tree.filter(treeItem => treeItem.parentId == item.id);
      if (children.length > 0) {
        this.queryTree(item, sortedEntries[sortedEntries.length - 1]['items']);
      }
      else {
        sortedEntries[sortedEntries.length - 1]['items'].push({
          content: item,
          items: []
        });
      }
    });

    if (sort) {
      sortedEntries[index]['items'] = sortedEntries[index]['items'].sort((a, b) => {
        const aSort = a.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');
        const bSort = b.content.name.toLowerCase().replace(/[^a-zA-Z\d]/g, '');

        if (aSort < bSort) {
          return -1;
        }
        if (aSort > bSort) {
          return 1;
        }
        return 0;
      });
    }
  }

  async fetchAll() {
    if (this.path.length < 1) {
      ui.notifications.error(game.i18n.localize('LK_INTEGRATION.error.noDir'));
      return false;
    }

    // Load the file dir.
    let dir = await FilePicker.browse(this.source, this.path);

    // Fetch the root index.
    let success = await this.fetchIndex();
    if (!success) {
      return false;
    }

    // Filter out unneeded files.
    let files = dir.files.filter(path => path.includes('index.json') == false && path.includes('json'));

    if (files.length > 0) {
      this.renderProgress({ current: 0, max: files.length - 1, details: 'LK_INTEGRATION.import.loadingFiles' });
      let count = 0;

      // Iterate through the filtered files.
      for (let file of files) {
        if (this.killAll) {
          return false;
        }

        await this.fetchJson(file, true);

        count++;
        this.renderProgress({ current: count, max: files.length - 1, details: 'LK_INTEGRATION.import.loadingFiles', update: true });
      }

      this.sorted = this.sortEntries();
    }

    return false;
  }

  async importEntries(items, parent = null, nestedParent = null, level = 0, prefix = '') {
    if (this.killAll) {
      return false;
    }

    if (items) {
      let count = 0;

      if (!parent) {
        await this.deleteEntries();
        game.LegendKeeper.updating = true;

        // The article import doesn't do a per article progress, so we're using
        // +10 here to show the user that there's still a significant step
        // left over while waiting on that task to complete.
        this.renderProgress({ current: 0, max: items.length + 10, details: 'LK_INTEGRATION.import.importingFolders', update: true });
        count = 0;
      }

      for (let item of items) {
        if (this.killAll) {
          return false;
        }

        let keys = Object.keys(item.items);
        if (keys.length > 0 || parent == null) {
          let levelPrefix = '';
          for (let i = 2; i < level; i++) {
            levelPrefix += '—';
          }

          if (level >= 3) {
            // The original version of this was useful for sorting, but it's disabled for now.
            // prefix = prefix == '' ? this.cleanPrefix(item.content.name) : `${prefix}/${this.cleanPrefix(item.content.name)}`;
            prefix = prefix == '' ? this.cleanPrefix(item.content.name) : `${prefix}${this.cleanPrefix(item.content.name)}`;
          }

          let folder = await Folder.create({
            // The original version of this was useful for sorting, but it's disabled for now.
            // name: `[LK]${level >= 2 ? '[|' + prefix + '|]' : ''} ${item.content.name}`,
            name: `[LK]${level >= 2 ? prefix : ''} ${item.content.name}`,
            type: 'JournalEntry',
            parent: parent,
            "flags.legendkeeper.id": item.content.id
          });

          await this.importEntry(item, folder.data._id, level, true);
          await this.importEntries(item.items, level < 2 ? folder.data._id : parent, folder.data._id, level + 1, prefix);
        }
        else {
          await this.importEntry(item, nestedParent, level);
        }

        if (!parent) {
          count++;
          this.renderProgress({ current: count, max: items.length + 10, details: 'LK_INTEGRATION.import.importingFolders', update: true });
        }
      }

      if (!parent) {
        if (this.scheduledUpdates.length > 0) {
          count++;
          this.renderProgress({ current: count, max: items.length + 10, details: 'LK_INTEGRATION.import.importingFolders', update: true });
          await JournalEntry.create(this.scheduledUpdates);
        }

        game.LegendKeeper.updating = false;
        this.dialog.close();
      }
    }
  }

  async importEntry(item, parent = null, level = 0, rootArticle = false) {
    if (this.killAll) {
      return false;
    }

    let content = item.content.documents.length > 0 ? item.content.documents.map(i => i.content).join('<hr>') : null
    let $content = [];
    if (content) {
      $content = $(`<div>${content}</div>`);
      let $secrets = $content.find('[data-extension-key="block-secret"]');
      if ($secrets.length > 0) {
        $secrets.each((index, element) => {
          let $self = $(element);
          if ($self.length > 0) {
            let html = $self.html();
            let $secret = $(`<section class="secret"></section>`);
            $secret.html(html);
            $self.replaceWith($secret[0]);
          }
        });
      }
    }

    this.scheduledUpdates.push({
      name: `[LK] ${rootArticle ? '[' + game.i18n.localize('LK_INTEGRATION.rootArticle') + ']------------------------------------' : item.content.name}`,
      content: $content && $content.length > 0 ? $content.html() : content,
      type: 'JournalEntry',
      folder: parent,
      "flags.legendkeeper.id": item.content.id,
      "rlags.legendkeeper.root": rootArticle,
    });
  }

  async deleteEntries() {
    // Query folders.
    let folders = game.folders.filter(f => f.type == 'JournalEntry' && f.parent == null && f.name.includes('[LK]'));
    this.renderProgress({ current: 0, max: folders.length - 1, details: 'LK_INTEGRATION.import.deletingEntries', update: true });
    let count = 0;

    for (let folder of folders) {
      if (this.killAll) {
        return false;
      }

      await folder.delete({ deleteSubfolders: true, deleteContents: true });
      count++;
      this.renderProgress({ current: count, max: folders.length - 1, details: 'LK_INTEGRATION.import.deletingEntries', update: true });
    }
    // Delete journal entries.
    let journals = game.journal.filter(j => j.name.includes('[LK]') && j.folder == null);
    for (let journal of journals) {
      await journal.delete();
    }
  };

  async updateLinks() {
    let journals = game.journal.filter(j => j.data.flags.legendkeeper != undefined);
    let updates = [];

    for (let journal of journals) {

      if (journal.data.content && journal.data.content.includes('.html')) {
        let content = $(journal.data.content);
        if (content.length > 0) {
          content.find('a').each((index, elem) => {
            let $self = $(elem);
            let url = $self.attr('href');
            let text = $self.text();
            let journalEntry = game.journal.find(j => j.data.flags?.legendkeeper?.id == url.split('.')[0]);
            if (journalEntry) {
              $self.replaceWith(`@JournalEntry[${journalEntry.data._id}]{${text}}`);
            }
            else {
              console.warn(`Unable to find linked entry for ${text} in article ${journal.data.name}. This entry was most likely deleted in your Legend Keeper world. Converting this link into plain text.`);
              $self.replaceWith(text);
            }
          });

          updates.push({
            _id: journal.data._id,
            content: content.html()
          });
        }
      }
    }

    if (updates.length > 0) {
      // Fake it.
      this.renderProgress({ current: 1, max: updates.length - 1, details: 'LK_INTEGRATION.import.updatingUrls', update: true });
      let count = 0;
      // game.LegendKeeper.updating = true;
      await JournalEntry.update(updates);
      // game.LegendKeeper.updating = false;
    }
  }

  renderProgress({ current = 0, max = 0, details = '', update = false }) {
    if (current == max) {
      return false;
    }

    if (!update) {
      this.dialog = new Dialog({
        title: 'Importing Legend Keeper...',
        content: `<div class="lk-progress-track"><div class="lk-progress-bar" style="width:${max > 0 ? (current / max) * 100 : 0}%;"></div></div>${details ? '<h2 class="lk-progress-title">' + game.i18n.localize(details) + '</h2>' : ''}`,
        buttons: {
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize("Cancel"),
            callback: () => null
          },
        },
        close: () => this.killAll = true,
      }, {
        width: 480,
        height: 140,
      }).render(true);
    }
    else {
      $(document).find('.lk-progress-bar').css('width', `${(current / max) * 100}%`);
      $(document).find('.lk-progress-title').text(game.i18n.localize(details));
    }
  }

  cleanPrefix(string) {
    // TODO: The original version of this function may be useful for sorting,
    // but for now, it's disabled.
    // return string.replace(/( +)/g, ' ').replace(/[^a-zA-Z\d ]/g, '').toUpperCase().split(' ').map(s => s[0]).join('');
    return '—';
  }
}